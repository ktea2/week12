<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Todo\TodoController;
use App\Http\Controllers\User\UserTodoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('auth')->group(function () {
    Route::post("register", [AuthController::class, 'register']);
    Route::post("login", [AuthController::class, "login"]);

    Route::post("logout", [AuthController::class, "logout"])->middleware(["auth:api"]);
});

Route::resource("todos", TodoController::class)->only(["index"]);
Route::group(['middleware' => 'auth:api'], function () {
    Route::resource("me/todos", UserTodoController::class);
});
